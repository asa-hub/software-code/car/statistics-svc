Asac Hub Talend job sheduler
-----------------------------

The following reoresents the crontab file. This file is maintained using the command: crontab -e. 
A local file can also be maintained and submitted using the command: crontab MyCrontabFile.cron. Note that this will replace the entire crontab file for the current user account.


--------------------BGIN OF CRON FILE CONTENT-----------------

16,31   8   *   *   1,2,3,4,5 /srv/hubasac/app/extract/cronjob/runTalendJob
1,16,31 7,9,10,11,12,13,14,15,16,17    *   *   1,2,3,4,5 /srv/hubasac/app/extract/cronjob/runTalendJob
1,21,51 17  *   *   1,2,3,4,5 /srv/hubasac/app/extract/cronjob/runTalendJob

-------------------END OF CRONE FILE CONTENT-------------------------


This crontab file is spécific to the Asac Hub extractor module Project. 
In this execution, we are scheduling a Job to run every 15 minutes, during insurance business opening time. 
This Job runs at 16 and 31 minutes past 8am and 1, 16 and 31 minutes past the hours of 9am, 10am, 11am, 12pm, 1pm, 2pm, 3pm. 
The 5pm schedule runs at 17:51 rather than 17:41, to catch the closing daily entries. This Job runs every weekday.


The auto generated Talend Job Control Script
When we exported our Talend Job, Talend creates a launch script, in our case it is called: extract_hubasac_run.sh

The following shows the content of the Talend-generated launch script for Asa-Hub extractor.


-------------------------- Talend job runner script---------------------------------------
#!/bin/sh
cd `dirname $0`
ROOT_PATH=`pwd`
java -Dtalend.component.manager.m2.repository=$ROOT_PATH/../lib -Xms256M -Xmx1024M -cp .:$ROOT_PATH:$ROOT_PATH/../lib/routines.jar:$ROOT_PATH/../lib/log4j-slf4j-impl-2.13.2.jar:$ROOT_PATH/../lib/log4j-api-2.13.2.jar:$ROOT_PATH/../lib/log4j-core-2.13.2.jar:$ROOT_PATH/../lib/commons-lang-2.6.jar:$ROOT_PATH/../lib/jboss-marshalling-2.0.12.Final.jar:$ROOT_PATH/../lib/talend_file_enhanced-1.1.jar:$ROOT_PATH/../lib/dom4j-2.1.3.jar:$ROOT_PATH/../lib/jakarta-oro-2.0.8.jar:$ROOT_PATH/../lib/slf4j-api-1.7.29.jar:$ROOT_PATH/../lib/postgresql-42.2.14.jar:$ROOT_PATH/../lib/talendcsv-1.0.0.jar:$ROOT_PATH/../lib/crypto-utils-0.31.12.jar:$ROOT_PATH/extract_hubasac_1_3.jar:$ROOT_PATH/extract_assure_1_3.jar:$ROOT_PATH/extract_attestation_1_3.jar:$ROOT_PATH/extract_remorque_1_3.jar:$ROOT_PATH/extract_vehicule_1_3.jar:$ROOT_PATH/extract_vente_1_3.jar:$ROOT_PATH/extract_sinistre_1_3.jar: extracteur_hubasac_backup.extract_hubasac_1_3.extract_hubasac --context=Default "$@"

------------------------------End of Talend job runner script------------------------


As can be seen from the crontab file, above, we are using a single Shell Script to execute and control our Jobs. 
This is a very basic script and we can enhance this as needed for any project. The current version of this script is shown below.


--------------------------------Start shell script---------------------------------------

#! /bin/bash
echo ""
echo "Welcome to the Talent cron job for Asac Hub Extractor"
echo ""
jobdir="/srv/hubasac/app/extract/1.3/extract_hubasac"
jobname="extract_hubasac_run"
echo ""
#--------please comment the next 20 lines ou if you intend to pass external parameters ------------
#
# jobdir=${1}
# jobname=${2}
#
# if [ ${#} -ne 2 ]; then
#        echo "runTalendJob: usage: project_folder job_name"
#        exit 2
# fi
#----------End of commented lines

echo "`date`: Starting Job $jobname" 1>> ${jobdir}/$jobname.log
#$jobdir/$jobname.sh 1>> $jobdir/$jobname.log 2>&1
$jobdir/$jobname.sh 1 | tee $jobdir/$jobname.log 2>&1
JobExitStatus=${?}

if [ ${JobExitStatus} -eq 0 ]; then
        echo "`date`: Job $jobdir.$jobname ended successfully" 1>> $jobdir/$jobname.log
else
        echo "`date`: Job $jobdir.$jobnae ended in error" 1>> $jobdir/$jobname.log
fi
exit ${JobExitStatus}

--------------------------------End shell script-------------------------------------------------------


This script assumes that our exported Job has been extracted to the directory /srv/hubasac/app/extract/1.3/ (which represents binary Home directory for the extract module of our Talend project.

The key elements to this script is that it allows us a simple naming convention for specifying the Job that we want to run, and output (stdout and stderr) is written to a log file.
