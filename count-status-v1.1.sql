
/*
select code_assureur,c_status, count(code_assure) as assure_count from assure  group by code_assureur, c_status order by code_assureur,c_status;
select c_status, count(code_assure) as assure_count from assure group by c_status;


select code_assureur,c_status, count(immatriculation) as vehicule_count from vehicule  group by code_assureur, c_status order by code_assureur,c_status;
select c_status, count(immatriculation) as Vehicule_count from vehicule group by c_status;

select code_assureur,c_status, count(immatriculation_remorque) as remorque_count from remorque  group by code_assureur, c_status order by code_assureur,c_status;
se[Mc))lect c_status, count(immatriculation_remorque) as Remorque_count from remorque group by c_status;

select code_assureur,c_status, count(numero_attestation) as vente_count from vente  group by code_assureur, c_status order by c_status;
select c_status,count(numero_attestation) as Vente_count from vente group by c_status;

select code_assureur,c_status, count(numero_attestation) as attestation_count from attestation  group by code_assureur, c_status order by c_status;
select c_status, count(immatriculation) as Attestation_count from attestation group by c_status;

select code_assureur,c_status, count(numero_attestation) as sinistre_count from sinistre  group by code_assureur, c_status order by c_status;
select c_status,count(numero_attestation) as Sinistre_count from sinistre group by c_status;
*/


/*### new##############################*/
SELECT code_assureur, c_status, COUNT(code_assure) AS assure_count FROM assure  RIGHT JOIN ref_assureur USING (code_assureur) GROUP BY code_assureur, c_status ORDER BY code_assureur, c_status ;

SELECT c_status, COUNT(code_assure) AS Assure_count FROM assure RIGHT JOIN ref_assureur USING (code_assureur) GROUP BY c_status ORDER BY c_status;

SELECT code_assureur, c_status, COUNT(immatriculation) AS vehicule_count FROM vehicule  RIGHT JOIN ref_assureur USING (code_assureur) GROUP  BY code_assureur, c_status ORDER BY code_assureur, c_status;

SELECT c_status, COUNT(immatriculation) AS Vehicule_count FROM vehicule RIGHT JOIN ref_assureur USING (code_assureur) GROUP BY c_status ORDER BY c_status;

SELECT code_assureur, c_status, COUNT(immatriculation_remorque) AS remorque_count FROM remorque  RIGHT JOIN ref_assureur USING (code_assureur) GROUP BY code_assureur, c_status ORDER BY code_assureur, c_status;

SELECT c_status, COUNT(immatriculation_remorque) AS Remorque_count FROM remorque RIGHT JOIN ref_assureur USING (code_assureur) GROUP BY c_status ORDER BY c_status;

SELECT code_assureur, c_status, COUNT(numero_attestation) AS vente_count FROM vente RIGHT JOIN ref_assureur USING (code_assureur) GROUP BY code_assureur, c_status ORDER BY code_assureur, c_status;

SELECT c_status, COUNT(numero_attestation) AS Vente_count FROM vente RIGHT JOIN ref_assureur USING (code_assureur) GROUP BY c_status ORDER BY c_status;

SELECT code_assureur, c_status, COUNT(numero_attestation) AS attestation_count FROM attestation RIGHT JOIN ref_assureur USING (code_assureur) GROUP BY code_assureur, c_status ORDER BY code_assureur, c_status;

SELECT c_status, COUNT(immatriculation) AS Attestation_count FROM attestation RIGHT JOIN ref_assureur USING (code_assureur) GROUP BY c_status ORDER BY c_status;

SELECT code_assureur, c_status, COUNT(reference) AS sinistre_count FROM sinistre  RIGHT JOIN ref_assureur USING (code_assureur) GROUP BY code_assureur, c_status ORDER BY code_assureur, c_status;

SELECT c_status, COUNT(reference)  AS Sinistre_count FROM sinistre  RIGHT JOIN ref_assureur USING (code_assureur) GROUP BY c_status ORDER BY c_status;
/*######## End New##################*/

