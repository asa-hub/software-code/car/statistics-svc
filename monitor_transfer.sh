#!/bin/bash

###set credentials
export PGPASSWORD="postgres"
server=localhost
database=hubasac
user=postgres
sqlScript=count-transfer-v1.1.sql
outputResult=output/resultTransfer

### csv formating wriite to file
psql -h $server -U $user -d $database -f $sqlScript -o $outputResult.csv -F ';' -A
#less $outputResult.csv

### Text formating write to console
psql -h $server -U $user -d $database -f $sqlScript -o $outputResult.txt
#less $outputResult.txt

