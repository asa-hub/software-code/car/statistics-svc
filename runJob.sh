#!/bin/bash
### Hubs Asac Bach processing statistics Monitor

clear

echo ""
echo ""
echo ""
echo "About to run the Counter monitoring script for All Entities sort by Insurers combined."
echo "....................................................................................."
sh ./monitor_counter.sh

echo ""
echo ""
echo ""
echo "About to run the Status monitoring script for All Entities Order by  status for all insures."
echo "............................................................................................"
sh ./monitor_status.sh

echo ""
echo ""
echo ""
echo ""
echo "About to run the transfer monitoring script for All Entities cont in status: 3&4."
echo "................................................................................"
sh ./monitor_transfer.sh

echo ""
echo ""
echo ""
echo "Transfering repors output file"
echo "----------------------------"
echo ""
current_time=$(date "+%Y.%m.%d-%H.%M.%S")
echo "Current Time : $current_time"
zip -r ~/hubasac_stats-data$current_time.zip output

ls -la ~/*.zip

