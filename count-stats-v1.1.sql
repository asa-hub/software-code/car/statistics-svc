/*
select code_assureur, count(code_assure) as "assure_status = all" from assure group by code_assureur order by code_assureur;
select code_assureur, count(code_assure) as "assure_status = 1" from assure where c_status=1 group by code_assureur order by code_assureur;
select code_assureur, count(code_assure) as "assure_status = 2" from assure where c_status=2 group by code_assureur order by code_assureur;
select code_assureur, count(code_assure) as "assure_status = -2" from assure where c_status=-2 group by code_assureur order by code_assureur;
select code_assureur, count(code_assure) as "assure_status = 3"  from assure where c_status=3 group by code_assureur order by code_assureur;
select code_assureur, count(code_assure) as "assure_status = 4"  from assure where c_status=4 group by code_assureur order by code_assureur;


select code_assureur, count(immatriculation) as "vehicule_status = all" from vehicule group by code_assureur order by code_assureur;
select code_assureur, count(immatriculation) as "vehicule_status = 1" from vehicule where c_status=1 group by code_assureur order by code_assureur;
select code_assureur, count(immatriculation) as "vehicule_status = 2" from vehicule where c_status=2 group by code_assureur order by code_assureur;
select code_assureur, count(immatriculation) as "vehicule_status = -2" from vehicule where c_status=-2 group by code_assureur order by code_assureur;
select code_assureur, count(immatriculation) as "vehicule_status = 3" from vehicule where c_status=3 group by code_assureur order by code_assureur;
select code_assureur, count(immatriculation) as "vehicule_status = 4" from vehicule where c_status=4 group by code_assureur order by code_assureur;


select code_assureur, count(immatriculation_remorque) as "remorque_status = all" from remorque group by code_assureur order by code_assureur;
select code_assureur, count(immatriculation_remorque) as "remorque_status = 1" from remorque where c_status=1 group by code_assureur order by code_assureur;
select code_assureur, count(immatriculation_remorque) as "remorque_status = 2" from remorque where c_status=2 group by code_assureur order by code_assureur;
select code_assureur, count(immatriculation_remorque) as "remorque_status = -2" from remorque where c_status=-2 group by code_assureur order by code_assureur;
select code_assureur, count(immatriculation_remorque) as "remorque_status = 3" from remorque where c_status=3 group by code_assureur order by code_assureur;
select code_assureur, count(immatriculation_remorque) as "remorque_status = 4" from remorque where c_status=4 group by code_assureur order by code_assureur;


select code_assureur, count(numero_attestation) as "vente_status = all" from vente group by code_assureur order by code_assureur;
select code_assureur, count(numero_attestation) as "vente_status = 1" from vente where c_status=1 group by code_assureur order by code_assureur;
select code_assureur, count(numero_attestation) as "vente_status = 2" from vente where c_status=2 group by code_assureur order by code_assureur;
select code_assureur, count(numero_attestation) as "vente_status = -2" from vente where c_status=-2 group by code_assureur order by code_assureur;
select code_assureur, count(numero_attestation) as "vente_status = 3" from vente where c_status=3 group by code_assureur order by code_assureur;
select code_assureur, count(numero_attestation) as "vente_status = 4" from vente where c_status=4 group by code_assureur order by code_assureur;



select code_assureur, count(numero_attestation) as "attestation_status = all" from attestation group by code_assureur order by code_assureur;
select code_assureur, count(numero_attestation) as "attestation_status = 1" from attestation where c_status=1 group by code_assureur order by code_assureur;
select code_assureur, count(numero_attestation) as "attestation_status = 2" from attestation where c_status=2 group by code_assureur order by code_assureur;
select code_assureur, count(numero_attestation) as "attestation_status = -2" from attestation where c_status=-2 group by code_assureur order by code_assureur;
select code_assureur, count(numero_attestation) as "attestation_status = 3" from attestation where c_status=3 group by code_assureur order by code_assureur;
select code_assureur, count(numero_attestation) as "attestation_status = 4" from attestation where c_status=4 group by code_assureur order by code_assureur;


select code_assureur, count(reference) as "sinistre_status = all" from sinistre group by code_assureur order by code_assureur;
select code_assureur, count(reference) as "sinistre_status = 1" from sinistre where c_status=1 group by code_assureur order by code_assureur;
select code_assureur, count(reference) as "sinistre_status = 2" from sinistre where c_status=2 group by code_assureur order by code_assureur;
select code_assureur, count(reference) as "sinistre_status = -2" from sinistre where c_status=-2 group by code_assureur order by code_assureur;
select code_assureur, count(reference) as "sinistre_status = 3" from sinistre where c_status=3 group by code_assureur order by code_assureur;
select code_assureur, count(reference) as "sinistre_status = 4" from sinistre where c_status=4 group by code_assureur order by code_assureur;
*/
/*###### New###############################################*/



 SELECT code_assureur, COUNT(code_assure) AS "assure_status = all" FROM assure RIGHT JOIN ref_assureur USING (code_assureur) GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(code_assure) AS "assure_status = 1" FROM assure RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 1 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(code_assure) AS "assure_status = 2" FROM assure RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 2 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(code_assure) AS "assure_status = -2" FROM assure RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = -2 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(code_assure) AS "assure_status = 3" FROM assure RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 3 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(code_assure) AS "assure_status = 4"  FROM assure RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 4 GROUP BY code_assureur ORDER BY code_assureur;
 
 SELECT code_assureur, COUNT(immatriculation) AS "vehicule_status = all" FROM vehicule RIGHT JOIN ref_assureur USING (code_assureur) GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(immatriculation) AS "vehicule_status = 1" FROM vehicule RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 1 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(immatriculation) AS "vehicule_status = 2" FROM vehicule RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 2 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(immatriculation) AS "vehicule_status = -2" FROM vehicule vehicule RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = -2 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(immatriculation) AS "vehicule_status = 3" FROM vehicule RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 3 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(immatriculation) AS "vehicule_status = 4" FROM vehicule RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 4 GROUP BY code_assureur ORDER BY code_assureur;
 
 SELECT code_assureur, COUNT(immatriculation_remorque) AS "remorque_status = all" FROM remorque RIGHT JOIN ref_assureur USING (code_assureur) GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(immatriculation_remorque) AS "remorque_status = 1" FROM remorque RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 1 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(immatriculation_remorque) AS "remorque_status = 2" FROM remorque RIGHT JOIN ref_assureur USING (code_assureur)  WHERE c_status = 2 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(immatriculation_remorque) AS "remorque_status = -2" FROM remorque WHERE c_status = -2 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(immatriculation_remorque) AS "remorque_status = 3" FROM remorque RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 3 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(immatriculation_remorque) AS "remorque_status = 4" FROM remorque RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 4 GROUP BY code_assureur ORDER BY code_assureur;
 
 SELECT code_assureur, COUNT(numero_attestation) AS "vente_status = all" FROM vente RIGHT JOIN ref_assureur USING (code_assureur) GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(numero_attestation) AS "vente_status = 1" FROM vente RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 1 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(numero_attestation) AS "vente_status = 2" FROM vente RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 2 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(numero_attestation) AS "vente_status = -2" FROM vente RIGHT JOIN ref_assureur USING (code_assureur)  WHERE c_status = -2 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(numero_attestation) AS "vente_status = 3" FROM vente RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 3 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(numero_attestation) AS "vente_status = 4" FROM vente RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 4 GROUP BY code_assureur ORDER BY code_assureur;
 
 SELECT code_assureur, COUNT(numero_attestation) AS "attestation_status = all" FROM attestation RIGHT JOIN ref_assureur USING (code_assureur) GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(numero_attestation) AS "attestation_status = 1" FROM attestation RIGHT OUTER JOIN ref_assureur USING (code_assureur) WHERE c_status = 1 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(numero_attestation) AS "attestation_status = 2" FROM attestation RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 2 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(numero_attestation) AS "attestation_status = -2" FROM attestation RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = -2 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(numero_attestation) AS "attestation_status = 3" FROM attestation RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 3 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(numero_attestation) AS "attestation_status = 4" FROM attestation RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 4 GROUP BY code_assureur ORDER BY code_assureur;
 
 SELECT code_assureur, COUNT(reference) AS "sinistre_status = all" FROM sinistre RIGHT JOIN ref_assureur USING (code_assureur) GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(reference) AS "sinistre_status = 1" FROM sinistre RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 1 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(reference) AS "sinistre_status = 2" FROM sinistre RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 2 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(reference) AS "sinistre_status = -2" FROM sinistre RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = -2 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(reference) AS "sinistre_status = 3" FROM sinistre RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 3 GROUP BY code_assureur ORDER BY code_assureur;
 SELECT code_assureur, COUNT(reference) AS "sinistre_status = 4" FROM sinistre RIGHT JOIN ref_assureur USING (code_assureur) WHERE c_status = 4 GROUP BY code_assureur ORDER BY code_assureur;
 



/*#### End New##################################*/
